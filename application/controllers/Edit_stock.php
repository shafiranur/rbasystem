<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_stock extends CI_Controller {

	public function index()
	{
		$data['konten']='v-editstock';
		$this->load->view('v-template', $data);
	}

}

/* End of file Edit-stock.php */
/* Location: ./application/controllers/Edit-stock.php */