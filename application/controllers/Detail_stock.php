<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_stock extends CI_Controller {

	public function index()
	{
		$data['konten']='v-detailstock';
		$this->load->view('v-template', $data);
	}

}

/* End of file Detail_stock.php */
/* Location: ./application/controllers/Detail_stock.php */