<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	public function index()
	{
		$data['konten']='v-form';
		$this->load->view('v-template', $data);
	}

}

/* End of file Form.php */
/* Location: ./application/controllers/Form.php */