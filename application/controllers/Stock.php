<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

	public function index()
	{
		$data['konten']='v-stock';
		$this->load->view('v-template', $data);
	}

}

/* End of file Stock.php */
/* Location: ./application/controllers/Stock.php */