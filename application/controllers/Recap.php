<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recap extends CI_Controller {

	public function index()
	{
		$data['konten']='v-recap';
		$this->load->view('v-template', $data);
	}

}

/* End of file Recap.php */
/* Location: ./application/controllers/Recap.php */