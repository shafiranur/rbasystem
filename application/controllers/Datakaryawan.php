<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datakaryawan extends CI_Controller {

	public function index()
	{
		$data['konten']='v-datakaryawan';
		$this->load->view('v-template', $data);
	}

}

/* End of file Datakaryawan.php */
/* Location: ./application/controllers/Datakaryawan.php */