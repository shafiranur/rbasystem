<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Detail Stock</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b>DU Bushing</b> Detail</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                  
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th><input type="checkbox" onClick="selectAll(this)"/></th>
                      <th>Project Number</th>
                      <th>Date</th>
                      <th>Amount</th>
                      <th>Total Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                      <a href="<?=base_url('index.php/edit_stock')?>">
                        <tr>
                          <td><input type="checkbox" class="singlechkbox" value="1" name="ceklis" /></td>
                          <td>298</td>
                          <td>19-9-19</td>
                          <td>120</td>
                          <td>120</td>
                        </tr>
                      </a>
                    <tr>
                      <td><input type="checkbox" class="singlechkbox" value="2" name="ceklis"/></td>
                      <td>299</td>
                      <td>10-10-19</td>
                      <td>80</td>
                      <td>200</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

</div>

<script type="text/javascript">

  function selectAll(source) {
    checkboxes = document.getElementsByName('ceklis');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
</script>