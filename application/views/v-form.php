<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Formulir RBA</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                      Pilih Jenis Formulir <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" tabindex="-1" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">Sakit/Izin</a>
                      <a class="dropdown-item" tabindex="-1" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Cuti</a>
                      <a class="dropdown-item" tabindex="-1" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-messages" role="tab" aria-selected="false">Lembur</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" tabindex="-1" href="#">Food and Beverages</a>
                      <a class="dropdown-item" tabindex="-1" href="#">Purchase Request</a>
                      <a class="dropdown-item" tabindex="-1" href="#">Peminjaman</a>

                    </div>
                  </li>
                </ul>
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-7">
                  <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                      <h3 style="color: darkblue;">Formulir Sakit/Izin</h3>
                        <form role="form" style="width: 1050px">
                          <div class="row">
                            <div class="col-sm-6">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Alasan</label>
                                <textarea type="text" class="form-control" placeholder="Enter ..."></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Department</label>
                                  <select class="form-control">
                                    <option>-- Select Department --</option>
                                    <option>Directur</option>
                                    <option>Finance</option>
                                    <option>Purchasing</option>
                                    <option>HRD</option>
                                    <option>Engineering</option>
                                    <option>Logistic</option>
                                    <option>Sales</option>
                                    <option>IT</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Waktu</label>
                                  <input type="time" class="form-control" rows="3" placeholder="Enter ...">
                                  <input type="time" class="form-control" rows="3" placeholder="Enter ...">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Lama Sakit (hari)</label>
                                  <input type="text" class="form-control" rows="3" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="exampleInputFile">Surat Dokter / RS (jika >1 hari)</label>
                                <div class="input-group">
                                  <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                  </div>
                                  <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Kirim Formulir</button>
                          </div>
                        </form>
                    </div>
                    <!-- jangan mempersulit diri sendiri, semangat! -->
                    <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">
                    <h3 style="color: darkblue;">Formulir Cuti</h3>
                       <form role="form" style="width: 1050px">
                          <div class="row">
                            <div class="col-sm-6">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Tanggal</label>
                                <input type="date" class="form-control" placeholder="Enter ...">
                                <input type="date" class="form-control" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Alasan</label>
                                <textarea type="text" class="form-control" placeholder="Enter ..."></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Jenis Cuti</label>
                                  <select class="form-control">
                                    <option>-- Select --</option>
                                    <option>Cuti Tahunan</option>
                                    <option>Cuti Menikah</option>
                                    <option>Cuti Hamil</option>
                                    <option>Cuti Berbayar</option>
                                  </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Department</label>
                                  <select class="form-control">
                                    <option>-- Select Department --</option>
                                    <option>Directur</option>
                                    <option>Finance</option>
                                    <option>Purchasing</option>
                                    <option>HRD</option>
                                    <option>Engineering</option>
                                    <option>Logistic</option>
                                    <option>Sales</option>
                                    <option>IT</option>
                                  </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Alamat</label>
                                  <textarea type="text" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Nomor</label>
                                  <input type="file" class="form-control" rows="3">
                              </div>
                            </div>
                          </div>
                          
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Kirim Formulir</button>
                          </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-messages" role="tabpanel" aria-labelledby="vert-tabs-messages-tab">
                      <h3 style="color: darkblue;">Formulir Lembur</h3>
                       <form role="form" style="width: 1050px">
                          <div class="row">
                            <div class="col-sm-6">
                              <!-- text input -->
                              <div class="form-group">
                                <label>Tanggal SPL</label>
                                <input type="date" class="form-control" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Lembur Pada : </label><br>
                                <div class="btn-group btn-group-toggle" data-toggle="buttons" style="width: 500px">
                                  <label class="btn bg-olive active">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked> Hari Kerja
                                  </label>
                                  <label class="btn bg-olive">
                                    <input type="radio" name="options" id="option2" autocomplete="off"> Hari Libur
                                  </label>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Tanggal SPL</label>
                                <input type="date" class="form-control" placeholder="Enter ...">
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Uraian Tugas Lembur</label>
                                <textarea type="text" class="form-control" placeholder="Enter ..."></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Department</label>
                                  <select class="form-control">
                                    <option>-- Select Department --</option>
                                    <option>Directur</option>
                                    <option>Finance</option>
                                    <option>Purchasing</option>
                                    <option>HRD</option>
                                    <option>Engineering</option>
                                    <option>Logistic</option>
                                    <option>Sales</option>
                                    <option>IT</option>
                                  </select>
                              </div>
                            </div>
                          </div><br><br>
                          <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <!-- Table -->
                            <table id="myTable" class="table">
                              <tr>
                                  <th rowspan="2">NIK</th>
                                  <th rowspan="2">Nama Karyawan</th>
                                  <th colspan="2">Jam Lembur</th>
                                  <th rowspan="2">Jumlah Jam Lembur</th>
                              </tr>
                              <tr>
                                <th>Mulai</th>
                                <th>Berakhir</th>
                              </tr>

                              <tr class="unit-table">
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                              </tr>
                              <tr class="unit-table">
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                              </tr>
                              <tr class="unit-table">
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="time">
                                </td>
                                <td>
                                  <input type="text">
                                </td>
                              </tr>
                            </table>
                          </div>

                          <button id="add_btn" class="btn btn-primary">Add new row</button>
                          <button type="submit" class="btn btn-primary">Kirim Formulir</button>               
                        </form>
                    </div>
                    <div class="tab-pane fade" id="vert-tabs-settings" role="tabpanel" aria-labelledby="vert-tabs-settings-tab">
                       Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>

<script type="text/javascript">
  $('.custom-control-input').click(function() {
    $('.custom-control-input').not(this).prop('checked', false);
});

  function addRow() {
  var table = document.getElementById('myTable');
  var columnLength = table.getElementsByTagName('tr')[0].children.length;
  var units = document.getElementsByClassName('unit-table');
  var tr = document.createElement('tr');
  tr.className = 'unit-table';
  for(var i = 0; i < columnLength; i++){
    var td = document.createElement('td');
    var text = document.createElement('input');
    text.type = 'text';
    td.appendChild(text);
    tr.appendChild(td);
  }
  table.appendChild(tr);
}

document.getElementById('add_btn').click(function(){
  addRow()
});
</script>
