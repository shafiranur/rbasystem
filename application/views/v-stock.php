<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Live Stock</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Data Stock RBA</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th><input type="checkbox" onClick="selectAll(this)"/></th>
                      <th>Part Numb.</th>
                      <th>Type</th>
                      <th>Dimension</th>
                      <th>Qty</th>
                      <th style="width: 200px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="cursor: pointer" onclick="detail('<?=base_url('index.php/edit_stock')?>')">
                      <td><input type="checkbox" class="singlechkbox" value="1" name="ceklis"/></td>
                      <td>183</td>
                      <td>DU Bushing</td>
                      <td>12mm</td>
                      <td>10</td>
                      <td>
                        <a href="<?=base_url('index.php/detail_stock')?>">
                          <button type="button" class="btn btn-block btn-success btn-sm">Detail Stock</button>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="singlechkbox" value="2" name="ceklis"/></td>
                      <td>183</td>
                      <td>DU Bushing</td>
                      <td>12mm</td>
                      <td>10</td>
                      <td>
                        <button type="button" class="btn btn-block btn-success btn-sm">Detail Stock</button>
                      </td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="singlechkbox" value="3" name="ceklis"/></td>
                      <td>183</td>
                      <td>DU Bushing</td>
                      <td>12mm</td>
                      <td>10</td>
                      <td>
                        <button type="button" class="btn btn-block btn-success btn-sm" >Detail Stock</button>
                      </td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="singlechkbox" value="4" name="ceklis"/></td>
                      <td>183</td>
                      <td>DU Bushing</td>
                      <td>12mm</td>
                      <td>10</td>
                      <td>
                        <button type="button" class="btn btn-block btn-success btn-sm">Detail Stock</button>
                      </td>
                    </tr>
                    <tr>
                      <td><input type="checkbox" class="singlechkbox" value="5" name="ceklis"/></td>
                      <td>183</td>
                      <td>DU Bushing</td>
                      <td>12mm</td>
                      <td>10</td>
                      <td>
                        <button type="button" class="btn btn-block btn-success btn-sm">Detail Stock</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

</div>
<script type="text/javascript">
  function detail(url){
    window.location.href = url
  }

  function selectAll(source) {
    checkboxes = document.getElementsByName('ceklis');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }
</script>
