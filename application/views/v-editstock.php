<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Stock</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Part Number</label>
                    <input type="email" class="form-control" id="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Type</label>
                    <input type="password" class="form-control" id="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Dimension</label>
                    <input type="password" class="form-control" id="">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Qty</label>
                    <input type="password" class="form-control" id="">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
                
              </form>
            </div>
          </div>
      </div><!-- /.container-fluid -->
    </section>

</div>