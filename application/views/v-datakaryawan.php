<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Karyawan RBA</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>NIK</th>
                      <th>Nama Karyawan</th>
                      <th>Jabatan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr style="cursor: pointer" onclick="detail('profil')">
                      <td>0987654321</td>
                      <td>Lolo</td>
                      <td>Sales Support</td>
                    </tr>
                    <tr style="cursor: pointer" onclick="detail(#)">
                      <td>0987654321</td>
                      <td>Lolo</td>
                      <td>Sales Support</td>
                    </tr>
                    <tr style="cursor: pointer" onclick="detail(#)">
                      <td>0987654321</td>
                      <td>Lolo</td>
                      <td>Sales Support</td>
                    <tr style="cursor: pointer" onclick="detail(#)">
                      <td>0987654321</td>
                      <td>Lolo</td>
                      <td>Sales Support</td>
                    </tr>
                    <tr style="cursor: pointer" onclick="detail(#)">
                      <td>0987654321</td>
                      <td>Lolo</td>
                      <td>Sales Support</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Alasan Ditolak :</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Kirim</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function detail(url){
    window.location.href = url
  }
</script>
